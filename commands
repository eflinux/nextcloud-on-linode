# Update server
sudo dnf update

# Create user
sudo useradd -mG wheel "username"

# Set hostname and hosts file
sudo hostnamectl set-hostname "domain name"
sudo vim /etc/hosts # add your ip followed by the domain

# Install utilities
sudo dnf install -y epel-release yum-utils unzip curl wget \
bash-completion policycoreutils-python-utils mlocate bzip2

# Update again
sudo dnf update

# Install Apache
sudo dnf install -y httpd

# Create Virtualhost
sudo vim /etc/httpd/conf.d/nextcloud.conf

# Virtualhost file content
<VirtualHost *:80>
  DocumentRoot /var/www/html/nextcloud/
  ServerName  your.server.com # replace with your domain

  <Directory /var/www/html/nextcloud/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>

  </Directory>
</VirtualHost>

# Enable Apache
sudo systemctl enable --now httpd

# Enable Remirepo
sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm

# Intall PHP Module
sudo dnf module reset php
sudo dnf module install php:remi-7.4
dnf update

# PHP Extensions
sudo dnf install -y php php-gd php-mbstring php-intl php-pecl-apcu php-mysqlnd php-opcache php-json php-zip php-bcmath php-gmp php-process php-redis php-imagick

# Install MariaDB
sudo dnf install -y mariadb mariadb-server

# Secure the MariaDB installation
sudo mysql_secure_installation

# Create MariaDB Database
sudo mysql -u root -p
CREATE DATABASE nextcloud;
GRANT ALL PRIVILEGES ON nextcloud.* TO 'username'@'localhost' IDENTIFIED BY 'password';
FLUSH PRIVILEGES;
quit

# Install and activate Redis
sudo dnf install -y redis
sudo systemctl enable --now redis

# Download, extract, move and set permissions for Nextcloud
wget https://download.nextcloud.com/server/releases/latest.zip
unzip latest.zip
sudo cp -R nextcloud/ /var/www/html/
sudo mkdir /var/www/html/nextcloud/data
sudo chown -R apache:apache /var/www/html/nextcloud
sudo systemctl restart httpd

# Firewall
sudo firewall-cmd --zone=public --add-service=http --permanent
sudo firewall-cmd --zone=public --add-service=https --permanent
sudo firewall-cmd --reload

# SELinux
sudo semanage fcontext -a -t httpd_sys_rw_content_t '/var/www/html/nextcloud/data(/.*)?'
sudo semanage fcontext -a -t httpd_sys_rw_content_t '/var/www/html/nextcloud/config(/.*)?'sudo semanage fcontext -a -t httpd_sys_rw_content_t '/var/www/html/nextcloud/apps(/.*)?'
sudo semanage fcontext -a -t httpd_sys_rw_content_t '/var/www/html/nextcloud/.htaccess'
sudo semanage fcontext -a -t httpd_sys_rw_content_t '/var/www/html/nextcloud/.user.ini'
sudo semanage fcontext -a -t httpd_sys_rw_content_t '/var/www/html/nextcloud/3rdparty/aws/aws-sdk-php/src/data/logs(/.*)?'
sudo restorecon -R '/var/www/html/nextcloud/'
sudo setsebool -P httpd_can_network_connect on

# COMPLETE THE NEXTCLOUD INSTALL ON THE BROWSER
#
# Edit the php.ini
sudo vim /etc/php.ini

# Edit these parameters in the php.ini file
 memory_limit = 512M
 upload_max_filesize = 200M
 max_execution_time = 360
 post_max_size = 200M
 date.timezone = Your Timezone

# Edit the opcache.ini file
sudo vim /etc/php.d/10-opcache.ini

# Edit these parameters in the 10-opcache.ini file
 opcache.enable=1
 opcache.interned_strings_buffer=8
 opcache.max_accelerated_files=10000
 opcache.memory_consumption=128
 opcache.save_comments=1
 opcache.revalidate_freq=1

# Edit the Nextcloud config.php file
sudo vim /var/www/html/nextcloud/config/config.php

# Add these paramteres to the Nextcloud config.php file
'default_phone_region' => 'ADD YOUR REGION HERE'
'memcache.distributed' => '\OC\Memcache\Redis',
'memcache.locking' => '\OC\Memcache\Redis',
'memcache.local' => '\OC\Memcache\APCu',
'redis' => array(
  'host' => 'localhost',
  'port' => 6379,
),

# Install Certbot
sudo dnf install certbot python3-certbot-apache mod_ssl

# Run Certbot
sudo certbot --apache -d yourdomain.com

# If you encounter an error, restart httpd
sudo systemctl restart httpd

# Rerun Certbot
sudo certbot --apache -d yourdomain.com

# Edit the nextcloud.conf file
sudo vim /etc/httpd/conf.d/nextcloud.conf

# Add these parameters for HTTP Strict Transport Security at the end of the file
<IfModule mod_headers.c>
Header always set Strict-Transport-Security "max-age=15552000; includeSubDomains"
</IfModule>

# Reboot the server
sudo reboot

# ENJOY YOUR NEW NEXTCLOUD INSTALLATION



 
